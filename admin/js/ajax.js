// Developer Name : Pritesh Singh
// Project Name : Construction Managment System
// Description : This JS file for add ajax call function


var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
localStorage.setItem('timezone', timezone);
var auth_token = localStorage.getItem('auth_token');
var ems_user_id = localStorage.getItem('user_id');
var user_timezone = localStorage.getItem('timezone');
var user_profile = localStorage.getItem('profile_img');
var user_username = localStorage.getItem('username');
var first_name = localStorage.getItem('first_name');
var role = localStorage.getItem('role');

function notification_alert(type, msg){
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });
      
    Toast.fire({
        type: type,
        title: msg
    })
}

function getUrlVars(param) {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var c = url.searchParams.get(param);
    return c;
}

$('#loginfrm').submit(function(e) {
        e.preventDefault();
        var username = $('#email').val();
        var password = $('#password').val();
        $.ajax({
            type: 'POST',
            url: 'api/login.php',
            data: JSON.stringify({ username: username, password: password }),
            dataType: "json",
            success: function(data) {
                console.log(data);
                var status = data.status;

                if (status == 1) {
                    window.location = 'index.php';
                    localStorage.setItem('user_id', data.user_id);
                    localStorage.setItem('auth_token', data.auth_token);
                    var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
                    localStorage.setItem('timezone', timezone);
                    
                    localStorage.setItem('username', data.first_name);
                    
                    localStorage.setItem('first_name', data.first_name);
                    localStorage.setItem('role', data.role);
                    
                } else {
                    notification_alert('error', data.message);
                }
            }
        });
    });
  function logout() {
    if (auth_token == null || auth_token == undefined) {
        $.ajax({
            type: 'POST',
            url: 'api/logout.php',
            data: JSON.stringify({ nulltoken: 'nulltoken' }),
            dataType: "json",
            success: function(data) {
                var path = window.location.pathname;
                var page = path.split("/").pop();
                if (page == 'index.php' || page == 'registration.php' || page == '') {

                } else {
                    window.location = '../index.php';
                }
                localStorage.removeItem('user_id');
                localStorage.removeItem('auth_token');
                localStorage.removeItem('timezone');
                localStorage.removeItem('profile_img');
                localStorage.removeItem('username');
                localStorage.removeItem('role');
            }
        });
    } else {
        $.ajax({
            type: 'POST',
            url: 'api/verify_user.php',
            data: JSON.stringify({ user_id: ems_user_id, auth_token: auth_token }),
            dataType: "json",
            success: function(data) {
                var status = data.status;
                if (status == 1) {
                    var path = window.location.pathname;
                    var page = path.split("/").pop();
                    if (page == 'index.php' || page == '') {
                        window.location = 'admin/login.php';
                    } else {

                    }
                } else {
                    window.location = '../login.php';
                    localStorage.removeItem('user_id');
                    localStorage.removeItem('auth_token');
                    localStorage.removeItem('timezone');
                    localStorage.removeItem('profile_img');
                    localStorage.removeItem('username');
                    localStorage.removeItem('role');
                }
            }
        });
    }
} 

function getdashboarddata(){
    $.ajax({
        type: 'POST',
        url: 'api/dashboard.php',
        data: JSON.stringify({dashboarddata: 'dashboarddata'}),
        dataType: "json",
        success: function(data) {
            
            $('#username').html(data.username);
            $('#mobile').html(data.mobile);
            $('#emailid').html(data.email);
            $('#totalpro').html(data.totalpro);
            $('#totalsub').html(data.totalsub);
            $('#totalcar').html(data.totalcar);
            $('#totalfb').html(data.totalfb);
            
        }
    });
}


function logout1(auth_token)
{
    if (auth_token == null || auth_token == 1) {
        $.ajax({
            type: 'POST',
            url: 'api/logout.php',
            data: JSON.stringify({ nulltoken: 'nulltoken' }),
            dataType: "json",
            success: function(data) {
                var path = window.location.pathname;
                var page = path.split("/").pop();
                if (page == 'login.php' || page == '') {

                } else {
                    window.location = 'login.php';
                }
                localStorage.removeItem('user_id');
                localStorage.removeItem('auth_token');
                localStorage.removeItem('timezone');
                localStorage.removeItem('profile_img');
                localStorage.removeItem('username');
                localStorage.removeItem('role');
            }
        });
    } 

}

$('#addnewuserform').submit(function(e) {

        e.preventDefault();

        var password = $('#password').val();
        var cpassword = $('#cpassword').val();
        if (password == cpassword) {
            var jsonData = {
                newmember: 'New Member',
                first_name: $('#first_name').val(),
                last_name: $('#last_name').val(),
                mobile: $('#mobile').val(),
                email: $('#email').val(),
                gender: $('#gender').val(),                
                password: $('#password').val()
            }
            $.ajax({
                type: 'POST',
                url: 'api/registration.php',
                data: JSON.stringify(jsonData),
                dataType: "json",
                success: function(data) {
                    var status = data.status;
                    var mymembers = '';
                    if (status == 1) {
                        window.location = "index.php";

                    } else {
                        notification_alert('error', data.message);
                    }
                }
            }); 
        }else{
            notification_alert('error', "Comfirm Password is Not Match.");
        }
        
    });


function getallcareers(){
    $.ajax({
        type: 'POST',
        url: 'api/career.php',
        data: JSON.stringify({ allissuecode: 'allissuecode'}),
        dataType: "json",
        success: function(data) {
            console.log(data);
            $('#candidate_datatable').DataTable().clear().draw();
            var newrelationshiprow = '';
             if (data.status == 1) {
             $.each(data.pin, function(i, row) {
                        $('#candidate_datatable').DataTable().row.add( [
                        row.c_id,
                        row.applyfor,
                        row.cname,
                        row.cphone,
                        row.cemail,
                        row.message,
                        row.upload_at,
                        "<a href='../website/images/resume/"+row.resume+"' class='btn btn-block btn-primary btn-sm'>Download</a>",
                    
                    ]).node().id = 'pin_'+row.c_id;
                    $('#candidate_datatable').DataTable().draw();

                });
            } else {
                newrelationshiprow += '<tr>' +
                    '<td colspan="5">No code Added</td>' +
                    '</tr>';
            }


             //$('#candidate_datatable tbody').html(newrelationshiprow);

        }
    });
}

function getallcontact(){
    $.ajax({
        type: 'POST',
        url: 'api/contact.php',
        data: JSON.stringify({ allissuecontact: 'allissuecontact'}),
        dataType: "json",
        success: function(data) {
            $('#contact_datatable').DataTable().clear().draw();
            var newrelationshiprow = '';
            if (data.status == 1) {
                $.each(data.con, function(i, row) {
                    if(row.c_status == 1){
                        var c_status = '<span class="label label-success">Active</span>';
                        
                    }else if(row.c_status==2){
                        var c_status = '<span class="label label-table label-danger">Closed</span>';
                       
                    }else if(row.c_status==3)
                    {
                        var c_status = '<span class="label label-table label-primary">Rejected</span>';
                    }
                    else
                    {
                       var c_status = '<span class="label label-table label-warning">Pending</span>'; 
                    }

                    $('#contact_datatable').DataTable().row.add( [
                        
                        row.c_enq_id,
                        row.c_name,
                        row.c_contact,
                        row.c_email,
                        row.c_service,
                        row.c_orgname,
                        row.c_reference,
                        row.c_message,
                        row.enq_at,
                        c_status,
                        // '<button type="button" class="btn btn-danger fa fa-trash" onclick="deletecode('+ row.c_enq_id + ');"></button>'+
                        // '<button type="button" class="btn btn-warning fa fa-plus" onclick="Isactive('+row.c_enq_id+');" ></button>' +
                        // '<button type="button" class="btn btn-warning fa fa-check" onclick="Approve('+row.c_enq_id+');" ></button>'+
                        // '<button type="button" class="btn btn-default fa fa-ban" onclick="Decline('+row.c_enq_id+');" ></button>'     
                    ]).node().id = 'con_'+row.c_enq_id;
                    $('#contact_datatable').DataTable().draw();

                });
            } else {
                newrelationshiprow += '<tr>' +
                    '<td colspan="5">No code Added</td>' +
                    '</tr>';
            }


            // $('#pin_datatable tbody').html(newrelationshiprow);

        }
    });
}

function getallfeedback(){
    $.ajax({
        type: 'POST',
        url: 'api/feedback.php',
        data: JSON.stringify({ allissuefeedback: 'allissuefeedback'}),
        dataType: "json",
        success: function(data) {
            $('#feedback_datatable').DataTable().clear().draw();
            var newrelationshiprow = '';
            if (data.status == 1) {
                var status =0;
                $.each(data.fb, function(i, row) {
                    if(row.c_status == 1){
                        status = '<span class="badge bg-success">Approve</span>';
                        
                    }else if(row.c_status==2){
                        status = '<span class="badge bg-danger">Rejected</span>';
                       
                    }
                    else
                    {
                       var status = '<span class="badge bg-warning">Pending</span>';
                   }
                    $('#feedback_datatable').DataTable().row.add( [
                        
                        row.c_fb_id,
                        row.c_name,
                        row.c_design,
                        row.c_contact,
                        row.c_email,
                        row.c_orgname,
                        '<img src="../website/images/feedback/'+row.profile+'" class="attachment-img">',
                        '<img src="../website/images/feedback/'+row.logo+'" class="attachment-img">',
                        row.c_message,
                        row.enq_at,
                        status,
                        '<button type="button" class="btn btn-block  bg-gradient-warning " onclick="Approve('+row.c_fb_id+')"><i class="fas fa-check"></i></button>'+
                        '<button type="button" class="btn btn-block  bg-gradient-danger " onclick="Decline('+row.c_fb_id+')"><i class="fas fa-ban"></i></button>'
                         
                    ]).node().id = 'con_'+row.c_fb_id;
                    $('#feedback_datatable').DataTable().draw();

                });
            } else {
                newrelationshiprow += '<tr>' +
                    '<td colspan="5">No code Added</td>' +
                    '</tr>';
            }


            // $('#pin_datatable tbody').html(newrelationshiprow);

        }
    });
}


function getallnotrealted(){
    $.ajax({
        type: 'POST',
        url: 'api/nrf.php',
        data: JSON.stringify({ allissuenrf: 'allissuenrf'}),
        dataType: "json",
        success: function(data) {
            $('#notrealtedform_datatable').DataTable().clear().draw();
            var newrelationshiprow = '';
            if (data.status == 1) {
             $.each(data.nrf, function(i, row) {
                        $('#notrealtedform_datatable').DataTable().row.add( [
                        row.id,
                        row.upload_at,
                        "<a href='../website/images/resume/notform/"+row.resume+"' class='btn btn-block btn-primary btn-sm'>Download</a>",
                        
                    
                    ]).node().id = 'nrf_'+row.id;
                    $('#notrealtedform_datatable').DataTable().draw();

                });
            } else {
                newrelationshiprow += '<tr>' +
                    '<td colspan="5">No code Added</td>' +
                    '</tr>';
            }

            // $('#pin_datatable tbody').html(newrelationshiprow);

        }
    });
}


function getallproject(){
    $.ajax({
        type: 'POST',
        url: 'api/project.php',
        data: JSON.stringify({ allissuepro: 'allissuepro'}),
        dataType: "json",
        success: function(data) {
            $('#project_datatable').DataTable().clear().draw();
            var newrelationshiprow = '';
            if (data.status == 1) {
             $.each(data.pro, function(i, row) {
                        $('#project_datatable').DataTable().row.add( [
                            
                        row.enq_id,
                        row.name,
                        row.orgname,
                        row.contact,
                        row.email,
                        row.service,
                        row.reference,
                        row.message,
                        row.enq_at,
                    
                    ]).node().id = 'pro_'+row.enq_id;
                    $('#project_datatable').DataTable().draw();

                });
            } else {
                newrelationshiprow += '<tr>' +
                    '<td colspan="5">No code Added</td>' +
                    '</tr>';
            }

            // $('#pin_datatable tbody').html(newrelationshiprow);

        }
    });
}


function getallsubemail(){
    $.ajax({
        type: 'POST',
        url: 'api/subemail.php',
        data: JSON.stringify({ allissuese: 'allissuese'}),
        dataType: "json",
        success: function(data) {
            $('#subemail_datatable').DataTable().clear().draw();
            var newrelationshiprow = '';
            if (data.status == 1) {
             $.each(data.se, function(i, row) {
                        $('#subemail_datatable').DataTable().row.add( [
                        row.sub_id,
                        row.subemail,
                        row.created_date,
                    
                    ]).node().id = 'se_'+row.sub_id;
                    $('#subemail_datatable').DataTable().draw();

                });
            } else {
                newrelationshiprow += '<tr>' +
                    '<td colspan="5">No code Added</td>' +
                    '</tr>';
            }

            // $('#pin_datatable tbody').html(newrelationshiprow);

        }
    });
}

function Approve(c_fb_id)
{
    $.ajax({
        type: 'POST',
        url: 'api/feedback.php',
        data: JSON.stringify({ approve: 'approve' ,c_fb_id:c_fb_id}),
        dataType: "json",
        success: function(data) {
            if (data.status == 1) {
                
                notification_alert('success', data.message);
                getallcodes();
                window.location = 'feedback.php';
                // $('#project_'+project_id).remove();

            } else {
                notification_alert('danger', data.message);
            }
        }
    });
   
}
function Decline(c_fb_id)
{
    $.ajax({
        type: 'POST',
        url: 'api/feedback.php',
        data: JSON.stringify({ rejected: 'rejected' ,c_fb_id:c_fb_id}),
        dataType: "json",
        success: function(data) {
            if (data.status == 1) {
                
                notification_alert('success', data.message);
                // $('#project_'+project_id).remove();

            } else {
                notification_alert('danger', data.message);
            }
        }
    });
}

 $("#addevents").on('submit',(function(e) { 

      $("#btn_rsub").attr('disabled','disabled');
      $("#btn_rsub").text("Please Wait....");
      e.preventDefault();
      
$.ajax({
           url: "api/uploadm.php",
           type: "POST",
           data:  new FormData(this),
           contentType: false,
           cache: false,
           processData:false,
           
            success: function(data)

                {
                    
                    var status = data.status;
                    var addevents = '';
                    if (status == 1) 
                    {
                
                     $("#rsucmsg").show();
                     $("#rsucmsg").text('Thank You. Your Inquiry Submitted');
                     $("#rerrmsg").hide();
                     $('#addevents')[0].reset();
                     setTimeout(function(){
                      $("#addevents").removeAttr('disabled');
                      $("#addevents").text("Send Message");
                      location.reload();
                      // $("#btn_rsub").removeAttr('disabled');
                      // $("#btn_rsub").text("Save");
                      // location.reload();
                    },4000);
                  }
                     else
                      {       
                        $("#rerrmsg").show();
                        $("#rerrmsg").text(data);
                        $("#rsucmsg").hide();
                        $("#btn_rsub").removeAttr('disabled');
                        $("#btn_rsub").text("Save");
                        
                        }
                        setTimeout(function(){
                        $("#addevents").removeAttr('disabled');
                        $("#addevents").text(data);
                        $("#btn_rsub").removeAttr('disabled');
                        //$("#btn_rsub").text("Save");
                        // clear_call_out_alert();
                        $("#rerrmsg").hide();
                        $("#rsucmsg").hide();
                        location.reload();
                      },4000);
                      }
                      
                      
                    });

               }));

 function getallevents(){
   $.ajax({
        type: 'POST',
        url: 'api/events.php',
        data: JSON.stringify({ allevents: 'allevents'}),
        dataType: "json",
        success: function(data) {
            $('#events_datatable').DataTable().clear().draw();
            var newrelationshiprow = '';
            if (data.status == 1) {
                var status =0;
                var button =0;
                $.each(data.ev, function(i, row) {
                    if(row.status == 1){
                        status = '<span class="badge bg-success">Active</span>';
                        button ='<button type="button" class="btn btn-block  bg-gradient-danger " onclick="Deactive('+row.e_id+')"><i class="fas fa-ban"></i></button>';
                    }else if(row.status==2){
                        status = '<span class="badge bg-danger">Deactive</span>';
                        button ='<button type="button" class="btn btn-block  bg-gradient-success " onclick="Active('+row.e_id+')"><i class="fas fa-check"></i></button>';
                       
                    }
                    else
                    {
                       var status = '<span class="badge badge-warning">Pending</span>';
                       var button ='<button type="button" class="btn bg-gradient-success btn-lg " onclick="Active('+row.e_id+')"><i class="fas fa-check"></i></button>'+
                        '<button type="button" class="btn bg-gradient-danger btn-lg" onclick="Decline('+row.e_id+')"><i class="fas fa-ban"></i></button>';
                   }
                    $('#events_datatable').DataTable().row.add( [
                        
                        row.e_id,
                        row.e_name,
                        row.e_des,
                        row.e_date,
                        '<ul class="list-inline">'+
                              '<li class="list-inline-item">'+
                                  '<img alt="Avatar" class="table-avatar" src="../website/images/events/'+row.e_img1+'" width="30%" height="auto">'+
                              '</li>'+
                              // '<li class="list-inline-item">'+
                              //     '<img alt="Avatar" class="table-avatar" src="../website/images/events/'+row.e_img2+'" width="20%" height="auto">'+
                              // '</li>'+
                              
                          '</ul>',                       
                       
                        status,
                        button,     
                                                                                
                        '<button type="button" class="btn bg-gradient-info btn-lg " onclick="editevnts('+row.e_id+')"><i class="fas fa-pencil-alt"></i></button>' +
                        '<button type="button" class="btn bg-gradient-danger btn-lg " onclick="deleteevents('+row.e_id+')"><i class="fas fa-trash"></i></button>'                        
                        // '<a class="btn btn-info btn-sm" href="update_events.php?id='+row.e_id+'"><i class="fas fa-pencil-alt"></i>Edit</a>'
                        //update_events.php?id='+row.e_id+'
                    ]).node().id = 'con_'+row.e_id;
                    $('#events_datatable').DataTable().draw();

                });
            } else {
                newrelationshiprow += '<tr>' +
                    '<td colspan="5">No code Added</td>' +
                    '</tr>';
            }


            // $('#pin_datatable tbody').html(newrelationshiprow);

        }
    });
}

 $("#updateevents").on('submit',(function(e) { 

      $("#btn_rsub").attr('disabled','disabled');
      $("#btn_rsub").text("Please Wait....");
      e.preventDefault();
      

        $.ajax({

           
            url: "api/update.php",
           type: "POST",
           data:  new FormData(this),
           contentType: false,
           cache: false,
           processData:false,
           
            success: function(data)
                {
                    var status = data.status;
                    var updateevents = '';
                    if (status == 1) 
                    {
                
                     $("#rsucmsg").show();
                     $("#rsucmsg").text('Thank You. Your Inquiry Submitted');
                     $("#rerrmsg").hide();
                     $('#updateevents')[0].reset();
                     setTimeout(function(){
                      $("#updateevents").removeAttr('disabled');
                      $("#updateevents").text("Send Message");
                      header("Location: events.php");
                      // location.reload();
                      // $("#btn_rsub").removeAttr('disabled');
                      // $("#btn_rsub").text("Save");
                      // location.reload();
                    },4000);
                  }
                     else
                      {       
                        $("#rerrmsg").show();
                        $("#rerrmsg").text(data);
                        $("#rsucmsg").hide();
                        $("#btn_rsub").removeAttr('disabled');
                        $("#btn_rsub").text("Update");
                        
                        }
                        setTimeout(function(){
                        $("#updateevents").removeAttr('disabled');
                        $("#updateevents").text(data);
                        $("#btn_rsub").removeAttr('disabled');
                        //$("#btn_rsub").text("Save");
                        // clear_call_out_alert();
                        $("#rerrmsg").hide();
                        $("#rsucmsg").hide();
                        location.reload();
                      },4000);
                      }
                      
                      
                    });

               }));

 function Active(e_id)
{
    $.ajax({
        type: 'POST',
        url: 'api/events.php',
        data: JSON.stringify({ active: 'active' ,e_id:e_id}),
        dataType: "json",
        success: function(data) {
            if (data.status == 1) {
                
                notification_alert('success', data.message);
                setInterval('location.reload()', 5000);
                // $('#project_'+project_id).remove();

            } else {
                notification_alert('danger', data.message);
            }
        }
    });
   
}
function Deactive(e_id)
{
    $.ajax({
        type: 'POST',
        url: 'api/events.php',
        data: JSON.stringify({ deactive: 'deactive' ,e_id:e_id}),
        dataType: "json",
        success: function(data) {
            if (data.status == 1) {
                
                notification_alert('success', data.message);
                setInterval('location.reload()', 5000);
                // $('#project_'+project_id).remove();

            } else {
                notification_alert('danger', data.message);
            }
        }
    });
}

function deleteevents(e_id){
    $.ajax({
        type: 'POST',
        url: 'api/events.php',
        data: JSON.stringify({ deleteevents: e_id }),
        dataType: "json",
        success: function(data) {
            if (data.status == 1) {
                notification_alert('success', data.message);
                setInterval('location.reload()', 5000);
                $('#events_'+e_id).remove();
            } else {
                notification_alert('danger', data.message);
            }
        }
    });
}


function editevnts(e_id){
    $.ajax({
        type: 'POST',
        url: 'api/events.php',
        data: JSON.stringify({ editevents: e_id }),
        dataType: "json",
        success: function(data) {

            if (data.status == 1) {
                $('#imageModal').modal('show');
                $('#e_id').val(data.e_id);
                $('#name').val(data.name);
                $('#des').val(data.des);
                $('#date').val(data.date);
                $('#e_img2').val(data.e_img1);
                
            } else {
                notification_alert('danger', data.message);
            }
        }
    });
}
<?php include 'header.php'; ?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
  <!--NAVBAR-->
            <!--===================================================-->
                <?php include 'navbar.php'; ?>
            <!--===================================================-->
  <!--END NAVBAR-->
     
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>All Events</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Events</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-10">
        </div>
        <div class="col-2">
           <button type="button" class="btn btn-block bg-gradient-info" data-toggle="modal" data-target="#myModal">Add Events</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
          <h4 class="title">Add Events Form</h4>
                      <form id="addevents" name="addevents" class="row" method="post" enctype="multipart/form-data">
                          <input type="hidden" name="addevents" value="addevents">   
                            <div class="messages"></div>
                            <div class="form-group col-md-12">
                                <input id="e_name" type="text" name="e_name" class="form-control" placeholder="Name*" required="required" data-error="Event Name is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <textarea id="e_des" name="e_des" class="form-control" placeholder="Event Description" rows="2"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <input id="e_date" type="text" name="e_date" class="form-control" placeholder="Plz insert that SEPTEMBER 2, 2019 only *" required="required" data-error="Orgnazation Name is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                            
                              <div class="form-group col-md-4">
                                <label>Image s :</label>

                              </div>
                              <div class="form-group col-md-8">
                              <input id='upload' name="upload[]" type="file" multiple="multiple" />
                              </div>
                             <!--  <div class="form-group col-md-4">
                                <label>Image 2 :</label>

                              </div>
                              <div class="form-group col-md-8">
                              <input type="file" name="e_img2" placeholder="Upload Your Image* only jpg,png,jpeg"  data-error="No Resume attached!" >
                              </div> -->
                              <!-- <div class="form-group col-md-4">
                                <label>Image 3 :</label>

                              </div>
                              <div class="form-group col-md-8">
                              <input type="file" name="e_img3" placeholder="Upload Your Image* only jpg,png,jpeg"  data-error="No Image attached!" >
                              </div>
                              <div class="form-group col-md-4">
                                <label>Image 4 :</label>

                              </div>
                              <div class="form-group col-md-8">
                              <input type="file" name="e_img4" placeholder="Upload Your Image* only jpg,png,jpeg"  data-error="No Image attached!" >
                              </div>
                              <div class="form-group col-md-4">
                                <label>Image 5 :</label>

                              </div>
                              <div class="form-group col-md-8">
                              <input type="file" name="e_img5" placeholder="Upload Your Image* only jpg,png,jpeg"  data-error="No Image attached!" >
                              </div>
                              <div class="form-group col-md-4">
                                <label>Image 6 :</label>

                              </div> 
                              <div class="form-group col-md-8">
                              <input type="file" name="e_img6" placeholder="Upload Your Image* only jpg,png,jpeg"  data-error="No Resume attached!" >
                              </div>-->
                              
                             <br>
                            
                             <div class="form-group col-md-12">
                               
                                <div class="help-block with-errors text-left">
                                  <label style="color: red; font-weight: bolder; display: none;" id="rerrmsg"></label>
                                  <label style="color: green; font-weight: bolder; display: none;" id="rsucmsg"></label>
                                </div>
                            </div>
                             <br>
                             <br>
                            <div class="col-md-12">
                                <button class="btn btn-block bg-gradient-info" id="btn_rsub" type="submit" name="submit" value="Submit"><span>Send Message</span></button>
                            </div>
                        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <!-- <h3 class="card-title">DataTable with minimal features & hover style</h3> -->
            </div>
            <!-- /.card-header -->
            <div class="card-body" style="overflow-x:auto;">
              <table id="events_datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  
                  <th>ID</th>
                  <th>Event Name</th>
                  <th>Event Dec.</th>
                  <th>Date</th>
                  <th>Image</th>
                  <th>Status</th>
                  <th>Status Action</th>
                  <th>Action</th>

                </tr>
                </thead>
                <tbody>
                
                </tbody>
                <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Event Name</th>
                  <th>Event Dec.</th>
                  <th>Date</th>
                  <th>Image</th>
                  <th>Status</th>
                  <th>Status Action</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <section>
   <div id="imageModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <form method="POST" id="updateevents" enctype="multipart/form-data">
    <div class="modal-header">
       <h4 class="modal-title">Edit Event Details</h4>
     <button type="button" class="close" data-dismiss="modal">&times;</button>
     
    </div>
    <div class="modal-body">
     <div class="form-group">
      <label>Event Name</label>
      <input type="text" name="e_name" id="name"  class="form-control" />
     </div>
     <div class="form-group">
      <label>Description</label>
      <input type="text" name="e_des" id="des"  class="form-control" />
     </div>
     <div class="form-group">
      <label>Date</label>
      <input type="text" name="e_date" id="date"  class="form-control" />
     </div>
     <div class="form-group">
      <label>Image Name</label>
      <input type="text" name="e_img1" id="e_img2" class="form-control" />
     </div>
     <div class="form-group">
      <label>if you want to change image plz select</label>
      <input type="file" name="myFile">
     </div>
    </div>
    

    <div class="help-block with-errors text-left">
                                  <label style="color: red; font-weight: bolder; display: none;" id="rerrmsg"></label>
                                  <label style="color: green; font-weight: bolder; display: none;" id="rsucmsg"></label>
                                </div>
    <div class="modal-footer">
     <input type="hidden" name="e_id" id="e_id" value="" />
     <button class="btn btn-block bg-gradient-info" id="btn_rsub" type="submit" name="btn_save_updates"><span>Edit</span></button>
     <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
    </div>                            
   </form>
  </div>
 </div>
</div>
 </section>
<!--Footer-->
            <!--===================================================-->
                <?php include 'footer.php'; ?>
            <!--===================================================-->
  <!--END Footer-->


 <script>
 getallevents();
   
 
  
</script>
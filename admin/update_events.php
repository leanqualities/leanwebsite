<?php include 'header.php'; 
  if (isset($_GET['id']) && !empty($_GET['id'])) {

      $cmd1 = "SELECT  * FROM `eventss` WHERE e_id = '".$_GET['id']."'";
      $result1 = $connect->query($cmd1);
      if ($result1->num_rows > 0) {
          $response['eventsstatus'] = 1;
          while ($row1 = $result1->fetch_assoc()) {
              $e_id = $row1['e_id'];
              $e_name = $row1['e_name'];

              $e_des = $row1['e_des'];
              $e_date = $row1['e_date'];
              $e_img1 = $row1['e_img1'];
             
               }
      }
  }
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
  <!--NAVBAR-->
            <!--===================================================-->
                <?php include 'navbar.php'; ?>
            <!--===================================================-->
  <!--END NAVBAR-->
     
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Update Events</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Update Events</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
 
    <section class="content">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Update Events</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body" id="eventview1">
              <h4 class="title">Update Events Form</h4>
                      <form id="updateevents" name="updateevents" class="row" method="post" enctype="multipart/form-data">
                          <input type="hidden" name="e_id" value="<?php echo $e_id; ?>">   
                            <div class="messages"></div>
                            <div class="form-group col-md-12">
                                <input id="e_name" type="text" name="e_name" class="form-control" placeholder="Name*" required="required" value="<?php echo $e_name; ?>" data-error="Event Name is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <textarea id="e_des" name="e_des" class="form-control"   rows="2"><?php echo $e_des; ?></textarea>
                                
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <input id="e_date" type="text" name="e_date" class="form-control" placeholder="Plz insert that format only * SEPTEMBER 2, 2019" value="<?php echo $e_date; ?>"required="required" data-error="Orgnazation Name is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                            
                              <div class="form-group col-md-4">
                                <label>Image 1 :</label>

                              </div>
                              <div class="form-group col-md-4">
                              <input type="file" name="e_img1" placeholder="Upload Your Image* only jpg,png,jpeg"   data-error="No Resume attached!" >
                              </div>
                              <div class="form-group col-md-4">
                                 <img src="../website/images/events/<?php echo $e_img1; ?>" width="80%" height="80%">
                                 <input type="hidden" name="e_img11" value="<?php echo $e_img1; ?>">
                              </div>
                              
                              
                              
                             <br>
                            
                             <div class="form-group col-md-12">
                               
                                <div class="help-block with-errors text-left">
                                  <label style="color: red; font-weight: bolder; display: none;" id="rerrmsg"></label>
                                  <label style="color: green; font-weight: bolder; display: none;" id="rsucmsg"></label>
                                </div>
                            </div>
                             <br>
                             <br>
                            <div class="col-md-12">
                                <button class="btn btn-block bg-gradient-info" id="btn_rsub" type="submit" name="btn_save_updates"><span>Send Message</span></button>
                            </div>
                        </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-2"></div>
        
      </div>
      
    </section>
    <section>
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<!--Footer-->
            <!--===================================================-->
                <?php include 'footer.php'; ?>
            <!--===================================================-->
  <!--END Footer-->


 
  
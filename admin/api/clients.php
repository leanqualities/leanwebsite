<?php

include '../config/dbconfig.php';

$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);



if (isset($obj->client_fname) && !empty($obj->client_fname)) {

$client_fname = $obj->client_fname;
$client_lname = $obj->client_lname;
$client_email = $obj->client_email;
$client_phone = $obj->client_phone;
$address = $obj->address;
$user_id = $obj->user_id;

$response = [];


$query1 = "INSERT INTO `clients`(`client_fname`, `client_lname`, `client_email`, `client_phone`, `address`, `created_at`, `user_id`) 
            VALUES ('$client_fname', '$client_lname', '$client_email', '$client_phone', '$address', '$current_date_time', '$user_id')";
$q1 = $connect->query($query1);
$last_id = $connect->insert_id;


if ($q1) {
    $response['status'] = 1;
    $response['message'] = 'Successfully Added';
    $response['client_name']= $client_fname.' '.$client_lname;
    $response['email'] = $client_email;
    $response['phone'] = $client_phone;
    $response['client_id'] = $last_id;
    $response['created_at'] = date("m-d-Y", strtotime($current_date_time));
} else {
    $response['status'] = 0;
    $response['message'] = 'Error In Adding';
}

echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}


if (isset($obj->allclients) && !empty($obj->allclients)) {

    $timezone  = $obj->user_timezone;
    $response = [];

    $d = 0;
    $cmd1 = "SELECT client_id, client_fname, client_lname, client_email, client_phone, created_at FROM clients ORDER BY client_id DESC";
    $result1 = $connect->query($cmd1);
    if ($result1->num_rows > 0) {
        $response['clientstatus'] = 1;
        while ($row1 = $result1->fetch_assoc()) {
            $response['clients'][$d]['client_id']   = $row1['client_id'];
            $response['clients'][$d]['client_name']  = utf8_encode($row1['client_fname'].' '.$row1['client_lname']);
            $response['clients'][$d]['email']      = $row1['client_email'];
            $response['clients'][$d]['created_at']     = date("m-d-Y", strtotime($row1['created_at']));
            $response['clients'][$d]['phone']      = $row1['client_phone'];
            $d++;
        }
    } else {
        $response['clientstatus'] = 0;
    }


    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}


if (isset($obj->deleteclient) && !empty($obj->deleteclient)) {

    $client_id  = $obj->deleteclient;

    $response = [];


    $cmd1 = "DELETE FROM `clients` WHERE client_id = '$client_id'";
    $q1 = $connect->query($cmd1);

    if ($q1) {
        $response['status'] = 1;
        $response['message'] = 'Successfully Deleted';
    } else {
        $response['status'] = 0;
        $response['message'] = 'Error In Deleting';
    }

    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}
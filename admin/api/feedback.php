<?php

include '../config/dbconfig.php';


$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);



if (isset($obj->allissuefeedback) && !empty($obj->allissuefeedback)) {

    // $timezone  = $obj->user_timezone;
    $response = [];
    //$user_id= $obj->user_id; `applyfor`, `cname`, `cphone`, `cemail`, `resume`, `message`, `upload_at 
    $d = 0;
    $cmd1 = "SELECT * FROM `feedback` ORDER BY c_fb_id DESC";
    $result1 = $connect->query($cmd1);
    if ($result1->num_rows > 0) {
        $response['status'] = 1;
        while ($row1 = $result1->fetch_assoc()) {
            $response['fb'][$d]['c_fb_id']   = $row1['c_fb_id'];
            $response['fb'][$d]['c_name']  = $row1['c_name'];
            $response['fb'][$d]['c_design']  = $row1['c_design'];
            $response['fb'][$d]['c_contact'] = $row1['c_contact'];
            $response['fb'][$d]['c_email'] = $row1['c_email'];
            $response['fb'][$d]['c_orgname']      = $row1['c_orgname'];
            $response['fb'][$d]['profile']  = $row1['profile'];
            $response['fb'][$d]['logo']  = $row1['logo'];
            $response['fb'][$d]['c_message']  = $row1['c_message'];
            $response['fb'][$d]['enq_at']  = $row1['enq_at'];
            $response['fb'][$d]['c_status']  = $row1['c_status'];


            $d++;
        }
    } else {
        $response['status'] = 0;
    }

     // print_r($response);
     // exit();

    // header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}
if (isset($obj->approve) && !empty($obj->approve)) {

    $c_fb_id  = $obj->c_fb_id;

    $response = [];

    // $s="select status from code where code_id=".$code_id;
    // $q=$connect->query($s);
    // $row=$q->fetch_assoc();
    // $s=$row['status'];
    // if($s==0)
    // {
    //     $status=1;
    // }
    
    $cmd1 = "UPDATE `feedback` SET `c_status`= 1 WHERE `c_fb_id`=".$c_fb_id;
    $q1 = $connect->query($cmd1);

    
    if ($q1) {
        $response['status'] = 1;
        $response['message'] = 'Successfully Approved';
    } else {
        $response['status'] = 0;
        $response['message'] = 'Error In Approving';
    }


    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}

if (isset($obj->rejected) && !empty($obj->rejected)) {

    $c_fb_id  = $obj->c_fb_id;

    $response = [];

    $cmd1 = "UPDATE `feedback` SET `c_status`= 2 WHERE `c_fb_id`=".$c_fb_id;
    $q1 = $connect->query($cmd1);

    
    if ($q1) {
        $response['status'] = 1;
        $response['message'] = 'Successfully Rejected';
    } else {
        $response['status'] = 0;
        $response['message'] = 'Error In Rejecting';
    }


    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}
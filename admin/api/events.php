<?php

include '../config/dbconfig.php';

$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);





if (isset($obj->allevents) && !empty($obj->allevents)) {

    $response = [];
    //$user_id= $obj->user_id; `applyfor`, `cname`, `cphone`, `cemail`, `resume`, `message`, `upload_at 
    $d = 0;
    $cmd1 = "SELECT * FROM `eventss` ORDER BY e_id DESC";
    $result1 = $connect->query($cmd1);
    if ($result1->num_rows > 0) {
        $response['status'] = 1;
        while ($row1 = $result1->fetch_assoc()) {
            $response['ev'][$d]['e_id']   = $row1['e_id'];
            $response['ev'][$d]['e_name']  = $row1['e_name'];
            $response['ev'][$d]['e_des']  = $row1['e_des'];
            $response['ev'][$d]['e_date'] = $row1['e_date'];
            $response['ev'][$d]['e_img1'] = $row1['e_img1'];            
            $response['ev'][$d]['status'] = $row1['status'];
           


            $d++;
        }
    } else {
        $response['status'] = 0;
    }



    // print_r($response);

    // header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}


if (isset($obj->deleteevents) && !empty($obj->deleteevents)) {

    $e_id  = $obj->deleteevents;

    $response = [];


    $cmd1 = "DELETE FROM `eventss` WHERE e_id = '$e_id'";
    $q1 = $connect->query($cmd1);

    if ($q1) {
        $response['status'] = 1;
        $response['message'] = 'Successfully Deleted';
    } else {
        $response['status'] = 0;
        $response['message'] = 'Error In Adding';
    }

    $cmd2 = "DELETE FROM `eventss1` WHERE es_id = '$e_id'";
    $q2 = $connect->query($cmd2);

    if ($q2) {
        $response['status'] = 1;
        $response['message'] = 'Successfully Deleted';
    } else {
        $response['status'] = 0;
        $response['message'] = 'Error In Adding';
    }

    // print_r($response);

    // header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}




if (isset($obj->active) && !empty($obj->active)) {

    $e_id  = $obj->e_id;

    $response = [];
    
    $cmd1 = "UPDATE `eventss` SET `status`= 1 WHERE `e_id`=".$e_id;
    $q1 = $connect->query($cmd1);

    
    if ($q1) {
        $response['status'] = 1;
        $response['message'] = 'Successfully Active';
    } else {
        $response['status'] = 0;
        $response['message'] = 'Error In Active';
    }


    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}

if (isset($obj->deactive) && !empty($obj->deactive)) {

    $e_id  = $obj->e_id;

    $response = [];

    $cmd1 = "UPDATE `eventss` SET `status`= 2 WHERE `e_id`=".$e_id;
    $q1 = $connect->query($cmd1);

    
    if ($q1) {
        $response['status'] = 1;
        $response['message'] = 'Successfully Deactive';
    } else {
        $response['status'] = 0;
        $response['message'] = 'Error In Deactive';
    }


    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}

if (isset($obj->eventid) && !empty($obj->eventid)) {

    $e_id   = $obj->eventid;     // $timezone  = $obj->user_timezone;
    $response = [];
    //$user_id= $obj->user_id; `applyfor`, `cname`, `cphone`, `cemail`, `resume`, `message`, `upload_at 
    $d = 0;
     $cmd1 = "SELECT * FROM `eventss` WHERE `e_id`=".$e_id;
    $result1 = $connect->query($cmd1);
    if ($result1->num_rows > 0) {
        $response['status'] = 1;
        while ($row1 = $result1->fetch_assoc()) {
           
            
            $response['events'][$d]['name']  = $row1['e_name'];
            $response['events'][$d]['des']  = $row1['e_des'];
            $response['events'][$d]['date']  = $row1['e_date'];
            $response['events'][$d]['image1']  =$row1['image'];
            
            
            
            $d++;
        }
    } else {
        $response['status'] = 0;
    }

     // print_r($response);
     // exit();

    // header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}

if (isset($obj->editevents) && !empty($obj->editevents)) {

    $e_id  = $obj->editevents;

    $response = [];
    //$user_id= $obj->user_id; `applyfor`, `cname`, `cphone`, `cemail`, `resume`, `message`, `upload_at 
    // $d = 0;
     $cmd1 = "SELECT * FROM `eventss` WHERE `e_id`=".$e_id;
    $result1 = $connect->query($cmd1);
    if ($result1->num_rows > 0) {
        $response['status'] = 1;
        while ($row1 = $result1->fetch_assoc()) {
           
            $response['e_id']  = $row1['e_id'];
            $response['name']  = $row1['e_name'];
            $response['des']  = $row1['e_des'];
            $response['date']  = $row1['e_date'];
            $response['e_img1']  =$row1['e_img1'];
            
            
            
            // $d++;
        }
    } else {
        $response['status'] = 0;
    }



    // print_r($response);

    // header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}

<?php

include '../config/dbconfig.php';

$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);


if (isset($obj->dashboarddata) && !empty($obj->dashboarddata)) {

    //timezone  = $obj->user_timezone;
    
    $response = [];



    $cmd4 = "SELECT count(enq_id) as totalpro FROM project_enquiry";
    $result4 = $connect->query($cmd4);
    if ($result4->num_rows > 0) {
        while ($row4 = $result4->fetch_assoc()) {
            $response['totalpro']  = $row4['totalpro'];
        }
    }

    $cmd5 = "SELECT count(sub_id) as totalsub FROM subemail";
    $result5 = $connect->query($cmd5);
    if ($result5->num_rows > 0) {
        while ($row5 = $result5->fetch_assoc()) {
            $response['totalsub']  = $row5['totalsub'];
        }
    }

    // $cmd6 = "SELECT * FROM users WHERE user_id='".$user_id."'" ;
    // $result6 = $connect->query($cmd6);
    // if ($result6->num_rows > 0) {
    //     while ($row6 = $result6->fetch_assoc()) {
    //         $response['username']  = $row6['first_name'].' '.$row6['last_name'];
    //         $response['mobile']  = $row6['mobile'];
    //         $response['email']  = $row6['email'];
            
    //     }
    // }

    $cmd7 = "SELECT count(c_id) as totalcar FROM applyform";
    $result7 = $connect->query($cmd7);
    if ($result7->num_rows > 0) {
        while ($row7 = $result7->fetch_assoc()) {
            $response['totalcar']  = $row7['totalcar'];
        }
    }

    $cmd8 = "SELECT count(c_fb_id) as totalfb FROM feedback";
    $result8 = $connect->query($cmd8);
    if ($result8->num_rows > 0) {
        while ($row8 = $result8->fetch_assoc()) {
            $response['totalfb']  = $row8['totalfb'];
        }
    }
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}
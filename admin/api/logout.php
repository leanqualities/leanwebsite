<?php

include '../config/dbconfig.php';

$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);



if( isset( $obj->auth_token ) && ! empty( $obj->auth_token ) ) {
    $cmd_token = "SELECT * FROM token WHERE auth_token = '".$obj->auth_token."'";
    $result_token = $connect->query($cmd_token);
    if ($result_token->num_rows > 0) {
        while($row_token = $result_token->fetch_assoc()) {
            $query = "DELETE FROM token WHERE auth_token = '".$obj->auth_token."'";
            $connect->query($query);
            $status = 1;
        }
        session_destroy();
    }else{
        $status = 0;
    }

    $response = [
        'status'  => $status
    ];
    
   
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}

if( isset( $obj->nulltoken ) && ! empty( $obj->nulltoken ) ) {
    session_start();
     $_SESSION = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000, $params['path'], $params['domain'], $params['secure'], $params["httponly"]);
        }
    $response = [
        'status'  => 1
    ];
    session_unset();
    clearstatcache();
    session_destroy();
   
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);

}
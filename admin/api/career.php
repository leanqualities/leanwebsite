<?php

include '../config/dbconfig.php';


$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);



if (isset($obj->allissuecode) && !empty($obj->allissuecode)) {

    $response = [];
    $d = 0;
    $cmd1 = "SELECT * FROM `applyform` ORDER BY c_id DESC";
    $result1 = $connect->query($cmd1);
    if ($result1->num_rows > 0) {
        $response['status'] = 1;
        while ($row1 = $result1->fetch_assoc()) {
            $response['pin'][$d]['c_id']   = $row1['c_id'];
            $response['pin'][$d]['applyfor']  = $row1['applyfor'];
            $response['pin'][$d]['cname'] = $row1['cname'];
            $response['pin'][$d]['cphone'] = $row1['cphone'];
            $response['pin'][$d]['cemail']      = $row1['cemail'];
            $response['pin'][$d]['resume']  = $row1['resume'];
            $response['pin'][$d]['message']  = $row1['message'];
            $response['pin'][$d]['upload_at']  = $row1['upload_at'];
            
            $d++;
        }
    } else {
        $response['status'] = 0;
    }

    
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}

<?php

include '../config/dbconfig.php';


$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);



if (isset($obj->allissuecontact) && !empty($obj->allissuecontact)) {

    // $timezone  = $obj->user_timezone;
    $response = [];
    //$user_id= $obj->user_id; `applyfor`, `cname`, `cphone`, `cemail`, `resume`, `message`, `upload_at 
    $d = 0;
    $cmd1 = "SELECT * FROM `contact` ORDER BY c_enq_id DESC";
    $result1 = $connect->query($cmd1);
    if ($result1->num_rows > 0) {
        $response['status'] = 1;
        while ($row1 = $result1->fetch_assoc()) {
            $response['con'][$d]['c_enq_id']   = $row1['c_enq_id'];
            $response['con'][$d]['c_name']  = $row1['c_name'];
            $response['con'][$d]['c_contact'] = $row1['c_contact'];
            $response['con'][$d]['c_email'] = $row1['c_email'];
            $response['con'][$d]['c_service']      = $row1['c_service'];
            $response['con'][$d]['c_orgname']  = $row1['c_orgname'];
            $response['con'][$d]['c_reference']  = $row1['c_reference'];
            $response['con'][$d]['c_message']  = $row1['c_message'];
            $response['con'][$d]['enq_at']  = $row1['enq_at'];
            $response['con'][$d]['c_status']  = $row1['c_status'];

            $d++;
        }
    } else {
        $response['status'] = 0;
    }

     // print_r($response);
     // exit();

    // header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}

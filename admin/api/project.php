<?php

include '../config/dbconfig.php';


$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);



if (isset($obj->allissuepro) && !empty($obj->allissuepro)) {

    // $timezone  = $obj->user_timezone;
    $response = [];
    //$user_id= $obj->user_id; `applyfor`, `cname`, `cphone`, `cemail`, `resume`, `message`, `upload_at 
    $d = 0;
    $cmd1 = "SELECT * FROM `project_enquiry` ORDER BY enq_id DESC";
    $result1 = $connect->query($cmd1);
    if ($result1->num_rows > 0) {
        $response['status'] = 1;
        while ($row1 = $result1->fetch_assoc()) {
            $response['pro'][$d]['enq_id']   = $row1['enq_id'];
            $response['pro'][$d]['name']  = $row1['name'];
            $response['pro'][$d]['orgname'] = $row1['orgname'];
            $response['pro'][$d]['contact'] = $row1['contact'];
            $response['pro'][$d]['email']      = $row1['email'];
            $response['pro'][$d]['service']  = $row1['service'];
            $response['pro'][$d]['reference']  = $row1['reference'];
            $response['pro'][$d]['message']  = $row1['message'];
            $response['pro'][$d]['enq_at']  = $row1['enq_at'];
            
            $d++;
        }
    } else {
        $response['status'] = 0;
    }

     // print_r($response);
     // exit();

    // header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}

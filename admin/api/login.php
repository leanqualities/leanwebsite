<?php
 session_start();
include '../config/dbconfig.php';

$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);



if( isset( $obj->username ) && ! empty( $obj->username ) ) {
    $username = $obj->username;
    $password =  $obj->password;

    $user_id= "";
    $auth_token = "";
    $response = [];

    $cmd = "SELECT user_id, first_name, last_name, email, password, role FROM users WHERE email = '$username'";
    $result = $connect->query($cmd);
    if ( $result->num_rows > 0 ) {
        while( $row = $result->fetch_assoc() ) {
            $username           = $row['email'];
            $first_name         = $row['first_name'];
            $last_name          = $row['last_name'];
            $user_id            = $row['user_id'];
            $password1          = $row['password'];
            $role_id            = $row['role'];
            if(password_verify($password, $password1)) {
                // print('asdasdasd');
                $message    = "Welcome ".$first_name. " ".$last_name." to EMS";
                $status     = true;		  		
                $auth_token = bin2hex(random_bytes(16));
                // $query = "UPDATE users SET last_login_data_time = '$current_date_time' WHERE user_id = '$user_id' ";
                // $connect->query($query);
                $query_session = "INSERT INTO token(auth_token, user_id, created_at) VALUES ('$auth_token', '$user_id', '$current_date_time')";
                $connect->query($query_session);
               
                //$_SESSION['role_id'] = $role_id;
                $_SESSION['loggedin'] = true;
                $_SESSION['role'] = $role_id;
                $_SESSION['user_id'] = $user_id;
                $response['message'] = "logedin";
                $response['status'] = 1;

                // print($_SESSION['loggedin']);

            }else{
                $message    = "Invalid Password";
                $status     = false;

                $response['message'] = $message;
                $response['status'] = $status;
            }
        }
    }else{
        $message    = "Invalid Username";
        $status     = false;

        $response['message'] = $message;
        $response['status'] = $status;
    }
    
   
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}
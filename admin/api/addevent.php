<?php
include '../config/dbconfig.php';

$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);
    
    $status = 1;
    $response = [];
 if(isset($_POST['submit'])){
    

    if(count($_FILES['upload']['name']) <= 2){
        //Loop through each file
        for($i=0; $i<count($_FILES['upload']['name']); $i++) {
          //Get the temp file path
            $tmpFilePath = $_FILES['upload']['tmp_name'][$i];
            $allowed = array("jpg" => "file/jpg", "jpeg" => "file/jpeg", "png" => "file/png");
            $filename = $_FILES['upload']['name'][$i];
            $filetype = $_FILES["upload"]["type"][$i];
            $filesize = $_FILES["upload"]["size"][$i];
            $target_file1 = "uploaded/" . $filename;
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if(!array_key_exists($ext, $allowed)) die("Please select a valid file format & Plz Try Again..");
            if (file_exists($target_file1)) die("Sorry, file already exists & Plz Try Again..");
            // Verify file size - 5MB maximum
            $maxsize = 5 * 1024 * 1024;
            if($filesize > $maxsize) die(' File size is larger than the allowed limit & Plz Try Again..');

                           //Make sure we have a filepath
            if($tmpFilePath != ""){

                //save the filename
                $shortname = (rand(10,100000)).'_event'.$_FILES['upload']['name'][$i];

                //save the url and the file
                // $filePath = "uploaded/" .(rand(10,100000)).'_event'.$_FILES['upload']['name'][$i];

                //Upload the file into the temp dir
                if(move_uploaded_file($tmpFilePath,"../../website/images/events/".$shortname)) {

                    $files[] = $shortname;
                    $e_name = $_POST["e_name"];
                    $e_des = $_POST["e_des"];
                    $e_date = $_POST["e_date"];
                    $e_img1 = $files[0];
                    $e_img2 = $files[1];

                    $query1 = "INSERT INTO `events`(`e_name`, `e_des`, `e_date`, `e_img1`, `e_img2`, `status`, `create_date`)
                VALUES (
                    '$e_name','$e_des' ,'$e_date', '$e_img1', '$e_img2', '$status','$current_date_time'
                )";
                    $q1 = $connect->query($query1);
                    $last_id = $connect->insert_id;


                if ($q1) {
                        // $response['status'] = 1;
                        $response = 'Successfully Added';
                    } else {
                        // $response['status'] = 0;
                        $response = 'Error In Adding & Plz Try Again..';
                    }

                }else {
                       $response = 'Sorry, there was an error uploading your file & Plz Try Again..';
                        }
              }

        
    }
    }else{
        $response = 'Select Only 2 File...';
    }

     echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
   
}
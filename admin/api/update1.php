<?php
include '../config/dbconfig.php';

$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);
    
 
$extr = explode(".", $_FILES["e_img1"]["name"]);
$extl = explode(".", $_FILES["e_img2"]["name"]);
$target_dir = "../../website/images/events/";

$newresumename = round(microtime(true)) .'_img1.'. end($extr);
$newlogoname = round(microtime(true)) .'_img2.'. end($extl);
$target_file1 = $target_dir . $newresumename;
$target_file2 = $target_dir . $newlogoname;
$status = 1;
$response = [];

if(isset($_FILES["e_img1"]) && $_FILES["e_img1"]["error"] == 0){
        $allowed = array("jpg" => "file/jpg", "jpeg" => "file/jpeg", "png" => "file/png");
        $filename = $_FILES["e_img1"]["name"];
        $filetype = $_FILES["e_img1"]["type"];
        $filesize = $_FILES["e_img1"]["size"];


    
        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die($response['message']="Please select a valid file format & Plz Try Again..");
        if (file_exists($target_file1)) die($response['message']="Sorry, file already exists & Plz Try Again..");
        // Verify file size - 5MB maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die($response['message'] =' File size is larger than the allowed limit & Plz Try Again..');

      if(isset($_FILES["e_img2"]) && $_FILES["e_img2"]["error"] == 0){
        $allowed = array("jpg" => "file/jpg", "jpeg" => "file/jpeg", "png" => "file/png");
        $filename = $_FILES["e_img2"]["name"];
        $filetype = $_FILES["e_img2"]["type"];
        $filesize = $_FILES["e_img2"]["size"];
    
        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die($response['message']="Please select a valid file format & Plz Try Again..");
        if (file_exists($target_file2)) die($response['message']="Sorry, file already exists & Plz Try Again..");
        // Verify file size - 5MB maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die($response['message'] =' File size is larger than the allowed limit & Plz Try Again..');  

// Check if $status is set to 0 by an error
if ($status == 0) {
    $response = 'Sorry, your file was not uploaded & Plz Try Again..';
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["e_img1"]["tmp_name"], "../../website/images/events/". $newresumename)) {
        if(move_uploaded_file($_FILES["e_img2"]["tmp_name"], "../../website/images/events/". $newlogoname)) {

        $e_name = $_POST["e_name"];
        $e_des = $_POST["e_des"];
        $e_date = $_POST["e_date"];
        $status = 0;
        
        $query1 = "INSERT INTO `events`(`e_name`, `e_des`, `e_date`, `e_img1`, `e_img2`, `status`, `create_date`)
                VALUES (
                    '$e_name','$e_des' ,'$e_date', '$newresumename', '$newlogoname', '$status','$current_date_time'
                )";
    $q1 = $connect->query($query1);
    $last_id = $connect->insert_id;


    if ($q1) {
        // $response['status'] = 1;
        $response = 'Successfully Added';
    } else {
        // $response['status'] = 0;
        $response = 'Error In Adding & Plz Try Again..';
    }
    

    }} else {
        $response = 'Sorry, there was an error uploading your file & Plz Try Again..';
    }
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
    
}
       } 
   }
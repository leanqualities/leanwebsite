 <?php
session_start();

class Database{
  
    // specify your own database credentials
    private $host, $username, $password, $database;
    public $conn;
    function __construct(){

        if (strpos($_SERVER['SERVER_NAME'], 'localhost') !== false) {
            $this->host        = "localhost";
            $this->username    = "root";
            $this->password    = "";
            $this->database    = "lean_web_site";
            
        }else{
            $this->host        = "localhost";
            $this->username    = "lean_web_site";
            $this->password    = "lean@123";
            $this->database    = "lean_web_site";
        }
        
        $this->conn = null;
   
        try{
            $this->conn = mysqli_connect($this->host, $this->username, $this->password, $this->database);
            
        }catch(mysqli_sql_exception $exception){
            echo "Connection error: " . $exception->getMessage();
        }
    }

    // get the database connection
    public function getConnection(){
        return $this->conn;
    }
}

    // get database connection
    $database = new Database();
    $connect = $database->getConnection();

?> 

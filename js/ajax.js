// Developer Name : Pritesh Singh
// Project Name : Construction Managment System
// Description : This JS file for add ajax call function


var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
localStorage.setItem('timezone', timezone);
// var auth_token = localStorage.getItem('auth_token');
// var ems_user_id = localStorage.getItem('user_id');
// var user_timezone = localStorage.getItem('timezone');
// var user_profile = localStorage.getItem('profile_img');
// var user_username = localStorage.getItem('username');
// var fname = localStorage.getItem('firstname');

function notification_alert(type, msg){
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });
      
    Toast.fire({
        type: type,
        title: msg
    })
}

function getUrlVars(param) {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var c = url.searchParams.get(param);
    return c;
}

$(document).ready(function() {
      // New Enquiry
    $('#projectform').submit(function(e) {
    alert("hi");
        e.preventDefault();

        var name = $('#name').val();
        var orgname = $('#orgname').val();
        var contact = $('#contact').val();
        var email = $('#email').val();
        if($name != null || $orgname != null || $contact != null || $email != null) {
            var jsonData = {
                newenquiry:"newenquiry",
                name: $('#name').val(),
                orgname: $('#orgname').val(),
                contact: $('#contact').val(),
                email: $('#email').val(),
                service: $('#service').val(),
                reference: $('#reference').val(),
                message:  $('#message').val()           
            }
            $.ajax({
                type: 'POST',
                url: 'api/project_enquiry.php',
                data: JSON.stringify(jsonData),
                dataType: "json",
                success: function(data) {
                    var status = data.status;
                    var mymembers = '';
                    if (status == 1) {
                       notification_alert('success', data.message);

                    } else {
                        notification_alert('error', data.message);
                    }
                }
            }); 
        }else{
            notification_alert('error', "all the filled are not filled.");
        }
        
    });  

});

  function getallfeedback1(){
    $.ajax({
        type: 'POST',
        url: 'api/feedback.php',
        data: JSON.stringify({ allissuefeedback: 'allissuefeedback'}),
        dataType: "json",
        success: function(data) {
            if (data.status == 1) {
                var len = data.length;
                    if(len > 0){
                      // $('#id').html(data[0]['c_fb_id']);
                      // $('#logo').html(data[0]['<img src="../website/images/feedback/'+row.logo+'" class="attachment-img">']);
                      // $('#profile').html(data[0]['<img src="../website/images/feedback/'+row.profile+'" class="attachment-img">']);
                      // $('#name').html(data[0]['c_name']);
                      // $('#designation').html(data[0]['c_design']);
                      // $('#message').html(data[0]['c_message']);
                        row.enq_id,
                        row.name,
                        row.orgname,
                        row.contact,
                        row.email,
                        row.service,
                        row.reference,
                        // var id = data[0]['c_fb_id'];
                        // var logo = data[0]['<img src="../website/images/feedback/'+row.logo+'" class="attachment-img">'];
                        // var profile = data[0]['<img src="../website/images/feedback/'+row.profile+'" class="attachment-img">'];
                        // var name = data[0]['c_name'];
                        // var designation = data[0]['c_design'];
                        // var message = data[0]['c_message'];

                        // document.getElementById('logo').value = logo;
                        // document.getElementById('profile').value = profile;
                        // document.getElementById('state').value = state;
                        // document.getElementById('name').value = name;
                        // document.getElementById('designation').value = city;
                        // document.getElementById('message').value = state;
                      }
            } else {
                
            }


            // $('#pin_datatable tbody').html(newrelationshiprow);

        }
    });
}

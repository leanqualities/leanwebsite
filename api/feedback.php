<?php

include '../config/dbconfig.php';


$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);



if (isset($obj->allissuefeedback) && !empty($obj->allissuefeedback)) {

    // $timezone  = $obj->user_timezone;
    $response = [];
    //$user_id= $obj->user_id; `applyfor`, `cname`, `cphone`, `cemail`, `resume`, `message`, `upload_at 
    $d = 0;
    $cmd1 = "SELECT * FROM `feedback` ORDER BY c_fb_id DESC";
    $result1 = $connect->query($cmd1);
    if ($result1->num_rows > 0) {
        $response['status'] = 1;
        while ($row1 = $result1->fetch_assoc()) {
            $response['testimonials'][$d]['id']   = $row1['c_fb_id'];
            $response['testimonials'][$d]['name']  = $row1['c_name'];
            $response['testimonials'][$d]['logo']  = $row1['logo'];
            $response['testimonials'][$d]['profile']  = $row1['profile'];
            $response['testimonials'][$d]['designation']  = $row1['c_design'];
            
            $response['testimonials'][$d]['message']  = $row1['c_message'];
            $d++;
        }
    } else {
        $response['status'] = 0;
    }

     // print_r($response);
     // exit();

    // header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}

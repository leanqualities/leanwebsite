<?php

include '../config/dbconfig.php';

$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);

if (isset($obj->newenquiry) && !empty($obj->newenquiry)) {

    $name = $obj->name;
    $orgname = $obj->orgname;
    $contact = $obj->contact;
    $email = $obj->email;
    $service =$obj->service;
    $reference = $obj->reference;
    $message = $obj->message;
    $response = [];

    $query1 = "INSERT INTO `project_enquiry`(`name`, `orgname`, `contact`, `email`, `service`, `reference`, `message`, `enq_at`) 
                VALUES (
                    '$name','$orgname', '$contact', '$email', '$service', '$reference', '$message','$current_date_time'
                )";
    $q1 = $connect->query($query1);
    $last_id = $connect->insert_id;


    if ($q1) {
        $response['status'] = 1;
        $response['message'] = 'Successfully Added';
    } else {
        $response['status'] = 0;
        $response['message'] = 'Error In Adding';
    }
    

    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}

if (isset($obj->newsubemail) && !empty($obj->newsubemail)) {

    $subemail = $obj->subemail;
    $response = [];

    $query1 = "INSERT INTO `subemail`(`subemail`, `created_date`) 
                VALUES (
                    '$subemail','$current_date_time')";
    $q1 = $connect->query($query1);
    $last_id = $connect->insert_id;


    if ($q1) {
        $response['status'] = 1;
        $response['message'] = 'Successfully Added';
    } else {
        $response['status'] = 0;
        $response['message'] = 'Error In Adding';
    }
    

    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}

  
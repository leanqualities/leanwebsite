<?php

include '../config/dbconfig.php';


$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);



if (isset($obj->allissueevents) && !empty($obj->allissueevents)) {

    // $timezone  = $obj->user_timezone;
    $response = [];
    //$user_id= $obj->user_id; `applyfor`, `cname`, `cphone`, `cemail`, `resume`, `message`, `upload_at 
    $d = 0;
    $cmd1 = "SELECT * FROM `eventss`";
    $result1 = $connect->query($cmd1);
    if ($result1->num_rows > 0) {
        $response['status'] = 1;
        while ($row1 = $result1->fetch_assoc()) {
            $response['events'][$d]['id']   = $row1['e_id'];
            $response['events'][$d]['name']  = $row1['e_name'];
            $response['events'][$d]['des']  = $row1['e_des'];
            $response['events'][$d]['date']  = $row1['e_date'];
            $response['events'][$d]['image1']  = $row1['e_img1'];
           
            
            
            $d++;
        }
    } else {
        $response['status'] = 0;
    }

     // print_r($response);
     // exit();

    // header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}


if (isset($obj->eventid) && !empty($obj->eventid)) {

    $e_id   = $obj->eventid;     // $timezone  = $obj->user_timezone;
    $response = [];
    //$user_id= $obj->user_id; `applyfor`, `cname`, `cphone`, `cemail`, `resume`, `message`, `upload_at 
    $d = 0;
    $cmd1 = "SELECT eventss.e_name,eventss.e_des,eventss.e_date,eventss1.image,eventss1.es_id FROM eventss LEFT JOIN eventss1 ON eventss.e_id=eventss1.es_id WHERE eventss1.es_id = ".$e_id." ORDER BY eventss1.e_id";
    $result1 = $connect->query($cmd1);
    if ($result1->num_rows > 0) {
        $response['status'] = 1;
        while ($row1 = $result1->fetch_assoc()) {
           
            
            $response['events'][$d]['name']  = $row1['e_name'];
            $response['events'][$d]['des']  = $row1['e_des'];
            $response['events'][$d]['date']  = $row1['e_date'];
            $response['events'][$d]['image1']  =$row1['image'];
            
            
            
            $d++;
        }
    } else {
        $response['status'] = 0;
    }

     // print_r($response);
     // exit();

    // header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}

if (isset($obj->eventid1) && !empty($obj->eventid1)) {

    $e_id   = $obj->eventid1;     // $timezone  = $obj->user_timezone;
    $response = [];
    //$user_id= $obj->user_id; `applyfor`, `cname`, `cphone`, `cemail`, `resume`, `message`, `upload_at 
    $d = 0;
    $cmd1 = "SELECT * FROM `eventss` WHERE `e_id`=".$e_id;
    $result1 = $connect->query($cmd1);
    if ($result1->num_rows > 0) {
        $response['status'] = 1;
        while ($row1 = $result1->fetch_assoc()) {
           
            
            $response['events'][$d]['name']  = $row1['e_name'];
            $response['events'][$d]['des']  = $row1['e_des'];
            
            $d++;
        }
    } else {
        $response['status'] = 0;
    }

     // print_r($response);
     // exit();

    // header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}
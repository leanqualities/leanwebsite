<?php
include '../config/dbconfig.php';

$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);
	
 
$extr = explode(".", $_FILES["resume"]["name"]);
$target_dir = "../website/images/resume/notform/";
$newresumename = round(microtime(true)) .'_resume.'. end($extr);
$target_file1 = $target_dir . $newresumename;
$status = 1;
$response = [];

if(isset($_FILES["resume"]) && $_FILES["resume"]["error"] == 0){
        $allowed = array("doc" => "file/doc", "docx" => "file/docx", "pdf" => "file/pdf");
        $filename = $_FILES["resume"]["name"];
        $filetype = $_FILES["resume"]["type"];
        $filesize = $_FILES["resume"]["size"];
    
        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die($response['message']="Please select a valid file format & Plz Try Again.");
        // $response['message']="Please select a valid file format & Plz Try Again.."
        if (file_exists($target_file1)) die($response['message']="Sorry, file already exists & Plz Try Again.");
        // $response['message']="Sorry, file already exists & Plz Try Again.."
        // Verify file size - 5MB maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die($response['message'] =' File size is larger than the allowed limit & Plz Try Again.');
        // $response['message'] =' File size is larger than the allowed limit & Plz Try Again..'

// Check if $status is set to 0 by an error
if ($status == 0) {
    $response = 'Something Went Wrong Please Try Again.';
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["resume"]["tmp_name"], "../website/images/resume/notform/". $newresumename)) {
        //$rem=$target_file. basename( $_FILES["resume"]["name"]);
        //$_token = $_POST["_token"];
    	//$resume = $rem;
    	
    	$query1 = "INSERT INTO `notrealtedform`(`resume`, `upload_at`) 
                VALUES ('$newresumename','$current_date_time')";
    $q1 = $connect->query($query1);
    $last_id = $connect->insert_id;


    if ($q1) {
        // $response['status'] = 1;
        $response = 'Successfully Added';
    } else {
         // $response['status'] = 0;
        $response = 'Error In Adding & Plz Try Again..';
    }
    

    } else {
        $response = 'Something Went Wrong Please Try Again.';
    }
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}
       } 
    


?>


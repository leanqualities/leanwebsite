<?php
include '../config/dbconfig.php';

$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);
    
 

$target_dir = "../feedback/";
$target_file = $target_dir . basename($_FILES["profile"]["name"]);
$status = 1;
$response = [];

if(isset($_FILES["profile"]) && $_FILES["profile"]["error"] == 0){
        $allowed = array("jpg" => "file/jpg", "png" => "file/png", "jpeg" => "file/jpeg");
        $filename = $_FILES["profile"]["name"];
        $filetype = $_FILES["profile"]["type"];
        $filesize = $_FILES["profile"]["size"];
    
        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die($response['message']="Please select a valid file format & Plz Try Again..");
        if (file_exists($target_file)) die($response['message']="Sorry, file already exists & Plz Try Again..");
        // Verify file size - 5MB maximum
        // $maxsize = 5 * 1024 * 1024;
        // if($filesize > $maxsize) die($response['message'] =' File size is larger than the allowed limit & Plz Try Again..');

// Check if $status is set to 0 by an error
if ($status == 0) {
    $response = 'Sorry, your file was not uploaded & Plz Try Again..';
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["profile"]["tmp_name"], $target_file)) {
        $rem=$target_file. basename( $_FILES["profile"]["name"]);
        $c_name = $_POST["c_name"];
    	$c_contact = $_POST["c_contact"];
    	$c_email = $_POST["c_email"];
    	
    	$c_orgname = $_POST["c_orgname"];
        $c_message = $_POST["c_message"];
    	$profile = $rem;
    	
    	$query1 = "INSERT INTO `applyform`(`applyfor`, `cname`, `cphone`, `cemail`, `resume`, `message`, `upload_at`) 
                VALUES (
                    '$c_orgname','$c_name', '$c_contact', '$c_email', '$profile', '$message','$current_date_time'
                )";
    $q1 = $connect->query($query1);
    $last_id = $connect->insert_id;


    if ($q1) {
        // $response['status'] = 1;
        $response = 'Successfully Added';
    } else {
        // $response['status'] = 0;
        $response = 'Error In Adding & Plz Try Again..';
    }
    

    } else {
        $response = 'Sorry, there was an error uploading your file & Plz Try Again..';
    }
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}
       } 
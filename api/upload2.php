<?php
include '../config/dbconfig.php';

$current_date_time = date("Y-m-d H:i:s");

$data = file_get_contents("php://input");
$obj = json_decode($data);
    
 
$extr = explode(".", $_FILES["resume"]["name"]);
$extl = explode(".", $_FILES["logo"]["name"]);
$target_dir = "../website/images/feedback/";

$newresumename = round(microtime(true)) .'_resume.'. end($extr);
$newlogoname = round(microtime(true)) .'_logo.'. end($extl);
$target_file1 = $target_dir . $newresumename;
$target_file2 = $target_dir . $newlogoname;
$status = 1;
$response = [];

if(isset($_FILES["resume"]) && $_FILES["resume"]["error"] == 0){
        $allowed = array("jpg" => "file/jpg", "jpeg" => "file/jpeg", "png" => "file/png");
        $filename = $_FILES["resume"]["name"];
        $filetype = $_FILES["resume"]["type"];
        $filesize = $_FILES["resume"]["size"];


    
        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die($response['message']="Please select a valid file format & Plz Try Again..");
        if (file_exists($target_file1)) die($response['message']="Sorry, file already exists & Plz Try Again..");
        // Verify file size - 5MB maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die($response['message'] =' File size is larger than the allowed limit & Plz Try Again..');

      if(isset($_FILES["logo"]) && $_FILES["logo"]["error"] == 0){
        $allowed = array("jpg" => "file/jpg", "jpeg" => "file/jpeg", "png" => "file/png");
        $filename = $_FILES["logo"]["name"];
        $filetype = $_FILES["logo"]["type"];
        $filesize = $_FILES["logo"]["size"];
    
        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die($response['message']="Please select a valid file format & Plz Try Again..");
        if (file_exists($target_file2)) die($response['message']="Sorry, file already exists & Plz Try Again..");
        // Verify file size - 5MB maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die($response['message'] =' File size is larger than the allowed limit & Plz Try Again..');  

// Check if $status is set to 0 by an error
if ($status == 0) {
    $response = 'Sorry, your file was not uploaded & Plz Try Again..';
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["resume"]["tmp_name"], "../website/images/feedback/". $newresumename)) {
        if(move_uploaded_file($_FILES["logo"]["tmp_name"], "../website/images/feedback/". $newlogoname)) {

        $c_name = $_POST["cname"];
        $c_design = $_POST["c_design"];
        $c_contact = $_POST["cphone"];
        $c_email = $_POST["cemail"];
        $c_orgname = $_POST["orgname"];
        $c_message = $_POST["message"];
        $applyfor = $_POST["applyfor"];
        // $profile = $rem;
        // $logo = $rem1;
        $c_status = 0;
        
        $query1 = "INSERT INTO `feedback`(`c_name`, `c_design`, `c_contact`, `c_email`, `c_orgname`, `profile`, `logo`, `c_message`, `c_status`, `enq_at`)
                VALUES (
                    '$c_name','$c_design' ,'$c_contact', '$c_email', '$c_orgname', '$newresumename', '$newlogoname', '$c_message', '$c_status','$current_date_time'
                )";
    $q1 = $connect->query($query1);
    $last_id = $connect->insert_id;


    if ($q1) {
        // $response['status'] = 1;
        $response = 'Successfully Added';
    } else {
        // $response['status'] = 0;
        $response = 'Error In Adding & Plz Try Again..';
    }
    

    }} else {
        $response = 'Sorry, there was an error uploading your file & Plz Try Again..';
    }
    echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
    
}
       } 
   }
    


?>

